const jwt = require("jsonwebtoken")
const config = require("../helper/config")
const Users = require("../model/user.model")
const loginCheck= (req,res,next)=>{
    let token = null
    if(req.headers['authorization']){
        token= req.headers['authorization']
    }
    if(req.headers['x-xsrf-token']){
        token = req.headers['x-xsrf-token']
    }
    if(req.headers['token']){
        token = req.headers['token']
    }
    if(req.query['token']){
        token = req.query['token']
    }
    if(!token){
        next({status:403, msg:"uathorized user"})
    }
    else{
        let splits = token.split(' ')
        if(splits.length>1){
            token = splits[1]
        }else{
            token = splits[0]
        }
        let data = jwt.verify(token,config.SECRET)
        if(data){
            let id = data._id
            Users.findById(id)
            .populate('role_id')
            .then((user)=>{
                if(user){
                    req.auth_user= user
                    next()
                }
                else{
                    next({msg:"user not found", status:404})
                }
            })
            .catch((error)=>{
                next({msg:"credential didnot match", status:400})
            })
        }else{
            next({msg:"token not valid"})
        }
    }
}
const adminAccess= (req,res,next)=>{
    let role = req.auth_user.role_id.name
    if(role.toLowerCase() == "admin" ){
        next()
    }
    else{
        next({msg:"permission denied"})
    }
}
const adminSeller=(req,res,next)=>{
    let role = req.auth_user.role_id
    if(role.name.toLowerCase()== "admin" || role.name.toLowerCase()== "seller"){
        next()
    }else{
        next({msg:"permission denied"})
    }
}
module.exports = {
    loginCheck,
    adminAccess,
    adminSeller
}