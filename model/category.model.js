const mongoose = require("mongoose")
const attribute = new mongoose.Schema({
    name: String,
    value: String
})

const categorySchema = mongoose.Schema({
    title: String,
    summary: String,
    image: String,
    brands: [{
        type: mongoose.Types.ObjectId,
        ref: "Brand",
        default: null
    }],
    parent_id:{
        type: mongoose.Types.ObjectId,
        ref: "Category",
        default: null
    },
    status:{
        type: String,
        enum: ['active','inactive'],
        default: 'inactive'
    }
},{
    timestamps:true
})
const Category = mongoose.model('Category',categorySchema)
module.exports = Category