const mongoose = require("mongoose")
const userSchema = new mongoose.Schema({
    name:{
        type: String,
        required: true
    },
    email:{
        type: String,
        required: true,
        unique: true
    },
    password:{
        type:String,
        required:true
    },
    status: {
        type: String,
        enum: ["active","inactive"],
        default: "inactive"
    },
    role: {
        type: String,
        enum:["admin","customer","seller"],
        default: "customer"
    },
    role_id: {
        type: mongoose.Types.ObjectId,
        ref: "Role"
    },
    reset_token:{
        type: String
    },
    // image: String,
    images: [String]
}, {
    timestamps: true
})
const Users = mongoose.model("Auth",userSchema)
 module.exports = Users
