const mongoose = require("mongoose")
const DiscountType = new mongoose.Schema({
    discount_type:{
        type:String,
        enum: ['percentage','amount']
    },
    discount_value:{
        type: Number
    }
})
const productSchema = mongoose.Schema({
    title:{
        type: String,
        require: true
    },
    summary:{
        type: String
    },
    description:{
        type:String
    },
    category:[{
        type: mongoose.Types.ObjectId,
        ref: "Category"
    }],
    price:{
        type:Number,
        req: true,
        min: 100
    },
    discount: DiscountType,
    after_discount: {
        type: Number
    },
    stock:{
        type:Number
    },
    min_order_qty:{
        type: Number
    },
    image: [String],
    is_featured:{
        type:Boolean,
        default: false
    },
    age_restricted:{
        type: Boolean,
        default: false
    },
    seller:{
        type: mongoose.Types.ObjectId,
        ref: "Users"
    },
    brands:{
        type: mongoose.Types.ObjectId,
        ref: "Brand"
    },
    status:{
        type:String,
        enum:['active','inactive'],
        default: 'inactive'
    }
},{
    timestamps: true
})
const ProductModel = mongoose.model('Product',productSchema)
module.exports = ProductModel
