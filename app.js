const express = require("express")
const app = express()
const cors = require("cors")
const apiRoutes = require("./router/index")
app.use(cors())
require("./service/mongoose")
app.use('/images',express.static(process.cwd()+'/upload'))
app.use(express.json())
app.use(express.urlencoded())


app.use(apiRoutes)

app.use((req,res,next)=>{
    req.json({
        msg: "not found",
        status: 400
    })
})
app.use((err,req,res,next)=>{
    let code = err?.status || 500
    let msg = err?.msg || "Error"
    res.status(code).json({
        result: req.body,
        msg: msg,
        status: false
    })
})
app.listen(9000,"localhost",(err)=>{
    if(err){
        console.log("unable to link with server")
    }
    else{
        console.log("connected with server successfully at port no:9000")
    }
})
