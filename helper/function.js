const fs = require("fs");
const apiResponse = (result= null, status= true, msg= "success")=>{
    let data={
        result: result,
        status: status,
        msg: msg
    }
    return data
}
const deleteImage = (image) => {
    return new Promise((resolve, reject) => {
        let path = process.cwd()+"/upload/"+image;
        
        try {
            fs.unlink(path, (err, success) => {
                if(err){
                    reject(err);
                } else {
                    resolve(success);
                }
            })
        } catch(err) {
            reject(err);
        }   

    })
}

module.exports = {apiResponse,deleteImage}
