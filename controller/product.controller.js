const {deleteImage} = require('../helper/function')
const ProductModel  = require('../model/product.model')
class ProductController{
    getAllProduct= (req,res,next)=>{
        ProductModel.find()
        .populate('category')
        .populate('brands')
        .then((products)=>{
            res.json({
                result: products,
                status: 200,
                msg: "all products found successfully"
            })
        })
        .catch((error)=>{
            res.json({msg:"product couldnot be found", status:400})
        })
    }
    addProduct = (req,res,next)=>{
        let data = req.body
        if(data.title== null){
            next({msg:"title is required"})
        }
        if(data.category.length == 0){
            data.category = []
        }
        let price = data.price;
        let after_discount = price;

        if(data.discount_type && data.discount_value){
            let dis_type = data.discount_type;
            let dis = data.discount_value;

            data.discount.discount_type = dis_type;
            data.discount.discount_value  = dis 
            
            if(dis_type == 'percentage'){
                after_discount = price - price * dis / 100;
            } else {
                after_discount = price-dis;
            }
        }
        data.after_discount = after_discount;
        if(req.files) {
            let images = [];

            req.files.map((o) => {
                images.push(o.filename)
            })
            
            data.images= images
        }

        if(req.auth_user.role_id.name == 'seller'){
            data.status = 'inactive';
        }
        let prod = new ProductModel(data);
        prod.save()
        .then((response) => {
            res.json({
                result: prod,
                status: true,
                msg: "product added successfully."
            })
        })
        .catch((err) => {
            next({status: 500, msg: JSON.stringify(err)})
        })


    }
    getProductById = (req,res,next)=>{
        ProductModel.findById(req.params.id)
        .populate('category')
        .populate('brands')
        .then((product) => {
            res.json({
                result: product,
                status: true,
                msg: "Product fetched"
            })
        })
        .catch((err) => {
            next({status: 500, msg: JSON.stringify(err)});
        })
    }
    updateProduct = (req,res,next)=>{
        let data = req.body;
        if(data.title == null){
            next({status: 400, msg: "Title is required."});
        }

        if(data.category.length == 0 ) {
            data.category =  [];
        }

        let price = data.price;
        let after_discount = price;

        if(data.discount_type && data.discount_value){
            let dis_type = data.discount_type;
            let dis = data.discount_value;

            data.discount.discount_type = dis_type;
            data.discount.discount_value  = dis 
            
            if(dis_type == 'percentage'){
                after_discount = price - price * dis / 100;
            } else {
                after_discount = price-dis;
            }
        }
        data.after_discount = after_discount;

        if(req.files) {
            let images = [];
            req.files.map((o) => {
                images.push(o.filename)
            })
            data.images= images
        }
        ProductModel.findByIdAndUpdate(req.params.id, {
            $set: data
        })
        .then((response) => {
            res.json({
                result: data,
                status: true,
                msg: "product updated successfully."
            })
        })
        .catch((err) => {
            next({status: 500, msg: JSON.stringify(err)})
        })

    }
    deleteProduct = (req,res,next)=>{
        ProductModel.findByIdAndDelete(req.params.id)
        .then((response) => {
            res.json({
                result: null,
                status: true,
                msg: "Product deleted Successfully"
            })
        })
        .catch((error) => {
            res.status(500).json({
                result: null,
                status: false,
                msg: JSON.stringify(error)
            })
        })
    }
}
module.exports = ProductController