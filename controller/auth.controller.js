const Users = require("../model/user.model")
const bcrypt= require("bcrypt")
const {apiResponse} = require("../helper/function")
const jwt = require("jsonwebtoken")
const config = require("../helper/config")
const nodemailer = require("nodemailer")
function generateToken(payload){
    let token = jwt.sign(payload,config.SECRET)
    return token
}

class authController{
    user
    mapUser =(data)=>{
            this.user = {
                _id: data._id,
                name: data.name,
                email: data.email,
                status: data.status,
                role_id: data.role_id,
                image: data.image,
                createdAt: data.createdAt,
                updatedAt: data.updatedAt
            }
    }


    login = (req,res,next)=>{
        Users.findOne({
            email: req.body.email
        })
        .populate("role_id")
        .then((user)=>{
            bcrypt.compare(req.body.password,user.password,(err,result)=>{
                if(result){
                    this.mapUser(user)
                    let token= generateToken({
                        _id: this.user._id,
                        email: this.user.email,
                        role: this.user.role_id.name
                    })
                    res.json({
                        result: {
                        user:this.user,
                        token: token,
                        },
                        status: 200
                    })
                }else{
                    next({msg: " credentail did not match"})
                }
            })
        })
        .catch((error)=>{
            next({msg: "user not found"})
        })
    }

    validation = (data)=>{
        let msg = []
        if(!data.email|| !data.password || !data.confirm_password ){
            msg.push("enter password and email")
        }
        if(data.password !== data.confirm_password){
            msg.push("password and confrim_password didnot matched")
        }
        if(!data.email.includes("@gmail.com")){
            msg.push("email must required @gmail.com")
        }
        if(msg.length>0){
            return{
                status: 400,
                msg: JSON.stringify(msg)
            }
        }
        else{
            return null
        }
    }

    register = (req,res,next)=>{
       let data = req.body
       const validate = this.validation(data)

       if(validate){
           next(validate)
       }
       else{
           if(req.file){
               data.images = req.file.filename
           }
           if(data.confirm_password){
               delete data.confirm_password
           }
           data.password = bcrypt.hashSync(data.password,10)
           let user = new Users(data)
           user.save()
           .then((success)=>{
               res.json({
                   result: user,
                   status:200,
                   msg: "successfully registered"
               })
           })
           .catch((error)=>{
               next({
                   msg: JSON.stringify(error)
               })
           })
       }
    }
    forgetPassword = (req,res,next)=>{
        Users.findOne({
            email: req.body.email
        })
        .then((user)=>{
            if(user){
                let token = generateToken({
                    _id: req.body._id
                })
            Users.findOneAndUpdate({
                email:req.body.email
            },{
                $set: {
                    reset_token: token
                }
            })
            .then((success)=>{
                let link = config.FE_URL+"/reset-password?token= "+token
                let transport = nodemailer.createTransport({
                    host: process.env.SMTP_HOST || config.SMTP_HOST ,
                    port: process.env.SMTP_PORT || config.SMTP_PORT,
                    auth: {
                      user: process.env.SMTP_USER || config.SMTP_USER,
                      pass: process.env.SMTP_PASS || config.SMTP_PASS
                    }
                  });
                transport.verify((err,success)=>{
                    console.log(err)
                    console.log(success)
                })
                let msg = `Dear ${user.name} <br/>`;
                msg += "You have requested for the password reset. Please click the link below or copy paste url in the browser: <br >";
                msg += `<a href='${link}'>${link}</a> <br />`;
                msg += "Regards,<br/>System Admin<br/>";
                msg += "<small><em>Please do not reply to this email</em></small>"

                transport.sendMail({
                    from: process.env.ADMIN_MAIL || config.ADMIN_MAIL,
                    to: req.body.email,
                    subject: "reset-password",
                    html: msg
                },(err,success)=>{
                    console.log(err)
                    console.log(success)
                })
            res.json({
                result: user,
                status:200,
                msg: "success"
            })
            })
            .catch((error)=>{
                next({msg: "cannot set token", status:400})
            })
            }
            else{
                next({msg: "User not found"})
            }
        })
        .catch((error)=>{
            next({msg: "not found", status:400})
        })
    }
    resetPassword = (req, res, next) => {
        let token = req.query.token;
        let password = req.body.password;
        let confirm = req.body.confirm_password;

        if(password != confirm || password.length < 8){
            next({status: 400, msg: "Password and confirm password must match and must be 8 character long"});
        }
        Users.findOne({
            reset_token: token
        })
        .then((user) => {
            if(user) {
                let pass = bcrypt.hashSync(password, 10);

                user.password = pass;

                user.save()
                .then((success) => {
                    res.json({
                        result: user,
                        status: 200,
                        msg: "Password updated successfully."
                    })
                }).catch((err) => {
                    next({
                        status: 400,
                        msg: "Error while updating password."
                    })
                })

            } else {
                next({status: 400, msg: "token expoired or already used"})
            }
        })
        .catch((error) => {
            next({status: 422, msg: "Error while db query"})
        })
    }

}
module.exports = authController
