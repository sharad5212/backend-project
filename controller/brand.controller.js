const BrandModel = require('../model/brand.model')
const {deleteImage} = require("../helper/function")
class BrandController{
    index=(req,res,next)=>{
        BrandModel.find()
        .then((brands)=>{
            if(brands.length){
                res.json({
                    result: brands,
                    status: true,
                    msg:"all brands found"
                })
            }else{
                res.json({
                    result: brands,
                    status: false,
                    msg: "brands not found"
                })
            }
        })
        .catch((error)=>{
            next({msg:JSON.stringify(error),status:422})
        })
    }
    store= (req,res,next)=>{
        let data = req.body
        if(req.file){
            data.image = req.file.filename
        let brands = new BrandModel(data)
        brands.save()
        .then((brands)=>{
            res.json({
                result: brands,
                status:200,
                msg: "brands updated successfully"
            })
        })
        .catch((error)=>{
            next({msg:JSON.stringify(error), status:422})
        })
    }else{
        next({msg: "file is required"})
    }
    }
    show= (req,res,next)=>{
        BrandModel.findById(req.params.id)
        .then((brand)=>{
            if(brand){
                res.json({
                    result: brand,
                    status:true,
                    msg: "brand found successfully"
                })
            }else{
                res.json({
                    result: brand,
                    status: false,
                    msg: "unable to find brand"
                })
            }
        })
        .catch((error)=>{
            next({msg: JSON.stringify(error), status:422})
        })
    }
    update= (req,res,next)=>{
        let data = req.body
        if(req.file){
            data.image = req.file.filename
        }
        BrandModel.findById(req.params.id)
        .then((brand)=>{
            if(brand.image && data.image){
                deleteImage(brand.image)
            }
            BrandModel.findByIdAndUpdate(req.params.id,{
                $set:data
            })
            .then((success)=>{
                res.json({
                    result:data,
                    status:200,
                    msg: "brand updated successfully"
                })
            })
            .catch((error)=>{
                next({msg: "brand not found", status:400})
            })
        })
        .catch((error)=>{
            next({msg:JSON.stringify(error),status: 422})
        })
    }
    destroy= (req,res,next)=>{
        BrandModel.findById(req.params.id)
        .then((brand)=>{
           if(brand){
            if(brand.image){
                deleteImage(brand.image)
            }
            BrandModel.findByIdAndDelete(req.params.id)
            .then((success)=>{
                res.json({
                    result:null,
                    status: 200,
                    msg: "brand deleted successfully"
                })
            })
            .catch((error)=>{
                next({msg: "error while deleting brand"})
            })
           }else{
               next({msg:"brand not found", status:400})
           }
        })
        .catch((error)=>{
            next({msg:JSON.stringify(error),status:422})
        })
    }
    getBrand = (req,res,next)=>{
        BrandModel.find({
            status: req.params.status
        })
        .then((brand)=>{
            if(brand){
                res.json({
                    result: brand,
                    status:200,
                    msg:"brand found successfully"
                })
            }else{
                res.json({
                    result:null,
                    status:400,
                    msg: "brand not found"
                })
            }
        }).catch((error)=>{
            next({msg:JSON.stringify(error), status:422})
        })
    }
}
module.exports = BrandController