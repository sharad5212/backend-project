const CategoryModel = require('../model/category.model')
const {deleteImage} =require("../helper/function")
class CategoryController{
    index = (req,res,next)=>{
        CategoryModel.find()
        .populate("parent_id")
        .populate("brands")
        .then((category)=>{
                res.json({
                    result: category,
                    status:true,
                    msg: "category found successfully"
                })
        })
        .catch((error)=>{
            res.json({msg:JSON.stringify(error), status:500})
        })
    }
    store = (req,res,next)=>{
        let data = req.body
        if(req.file){
            data.image = req.file.filename
        }
        let category = new CategoryModel(data)
        category.save()
        .then((category)=>{
            res.json({
                result: category,
                status:200,
                msg: "category saved successfully"
            })
        })
        .catch((error)=>{
            next({msg:JSON.stringify(error),status:500})
        })
    }
    show = (req,res,next)=>{
        CategoryModel.findById(req.params.id)
        .populate("parent_id")
        .populate("brands")
        .then((category)=>{
            res.json({
                result: category,
                status: 200,
                msg: "category found successfully"
            })
        })
        .catch((error)=>{
            res.json({msg:JSON.stringify(error), status:400})
        })
    }
    update = (req,res,next)=>{
        let data = req.body
        if(req.file){
            data.image = req.file.filename
        }
        CategoryModel.findById(req.params.id)
        .then((category)=>{
            if(category.image && data.image){
                deleteImage(category.image)
            }
            CategoryModel.findByIdAndUpdate(req.params.id,{
                $set: data
            })
            .then((success)=>{
                res.json({
                    result: data,
                    status:200,
                    msg: "category updated successfully"
                })
            })
            .catch((error)=>{
                next({msg:"unable to update category", status:400})
            })
        })
        .catch((error)=>{
            next({msg:JSON.stringify(error),status: 422})
        })
    }
    destroy= (req,res,next)=>{
        CategoryModel.findById(req.params.id)
        .then((category)=>{
            if(category.image){
                deleteImage(category.image)
            }
            CategoryModel.findByIdAndDelete(req.params.id)
            .then((success)=>{
                res.json({
                    result:null,
                    status:200,
                    msg:"category deleted successfully"
                })
            })
            .catch((error)=>{
                next({msg:"unable to delete category" ,status:400})
            })
        })
        .catch((error)=>{
            next({msg: JSON.stringify(error), status:422})
        })
    }
}
module.exports = CategoryController