const Users = require("../model/user.model")
const {apiResponse}  = require("../helper/function")
const Role = require("../model/role.model")
class UserController{
    getAllUsers = (req,res,next)=>{
        Users.find({
            _id:{$ne:req.auth_user._id}
        })  
        .populate("role_id")
        .then((users)=>{
          if(users){
              res.json({
                  result: users,
                  status:200,
                  msg: "users found successfully"
              })
          }else{
              res.json({
                  result: null,
                  status: 404,
                  msg:"user not found"
              })
          }
        })
        .catch((error)=>{
            next({msg:"didnot match", status:422})
        })
    }
    getProfile = (req,res,next)=>{
        Users.findById({
            _id: req.params.id
        })
        .populate("role_id")
        .then((user)=>{
            if(user){
                res.json({
                    result: user,
                    status: 200,
                    msg: "user found successfully"
                })
            }else{
                res.json({msg:"user not found", status:400})
            }
        })
        .catch((error)=>{
           res.json({msg: "credential didnot match", status:422})
        })
    }
    validation=(data)=>{
        let msg = []
        if(!data.email){
            msg.push("email is required")
        }
        if(!data.email.includes("@gmail.com")){
            msg.push("email must required @gmail.com")
        }
        if(msg.length>1){
            return{
                status:400,
                msg: JSON.stringify(msg)
            }
        }else{
            return null
        }
    }
    updateProfile = (req,res,next)=>{
        let data = req.body
        let validate  = this.validation(data)
        if(validate){
            next(validate)
        }else{
            if(req.file){
                data.images = req.file.filename
            }
            Users.findByIdAndUpdate({
                _id: req.params.id
            },{
                $set:data
            })
            .then((user)=>{
                if(user){
                    res.json({
                        result:user,
                        status:200,
                        msg:"user updated successfully"
                    })
                }else{
                    next({msg:"error while updating user", status:400})
                }
            })
            .catch((error)=>{
                next({msg:"credential didnot match", status:422})
            })
        }
    }
    deleteProfile = (req,res,next)=>{
        Users.findByIdAndDelete({
            _id: req.params._id
        })
        .then((success)=>{
            if(success){
                res.json({
                    result: null,
                    msg:"successfully deleted user",
                    status:200
                })
            }else{
                next({msg:"error while deleting user", status:400})
            }
        })
        .catch((error)=>{
            next({msg:"error in query", status:422})
        })
    }
}
module.exports = UserController