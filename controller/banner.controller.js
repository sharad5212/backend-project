const BannerModel = require("../model/banners.model")
 const {deleteImage} =require("../helper/function")
class BannerController{
    index=(req,res,next)=>{
        BannerModel.find()
        .then((banner)=>{
            if(banner.length){
                res.json({
                    result: banner,
                    status: true,
                    msg: "banner found successfully"
                })
            }else{
                res.json({
                    result: banner,
                    status:false,
                    msg: "user not found"
                })
            }
        })
        .catch((error)=>{
            next({msg:JSON.stringify(error)})
        })
    }
    store= (req,res,next)=>{
        let data = req.body
        if(req.file){
            data.image = req.file.filename
        }
        console.log(data.image)
        let banner = new BannerModel(data)
        banner.save()
        .then((banner)=>{
            if(banner){
                res.json({
                    result: banner,
                    status:200,
                    msg: "banner updated successfully"
                })
            }else{
                next({msg:"unable to store banner", status: 400})
            }
        })
        .catch((error)=>{
            next({msg:JSON.stringify(error), status: 422})
        })
    }
   show = (req, res, next) => {
        BannerModel.findById(req.params.id)
            .then((banner) => {
                if (banner) {
                    res.json({
                        result: banner,
                        status: true,
                        msg: "List success"
                    })
                } else {
                    res.json({
                        result: banner,
                        status: false,
                        msg: "Banner not found"
                    })
                }
            })
            .catch((err) => {
                next({ status: 422, msg: JSON.stringify(err) })
            })
    }

      update = (req, res, next) => {
        try {
            let data = req.body;

            // run vaidation 
            if (req.file) {
                data.image = req.file.filename;
            }

            BannerModel.findById(req.params.id)
                .then((banner) => {
                    if (banner.image && data.image) {
                        deleteImage(banner.image)
                    }
                    BannerModel.findByIdAndUpdate(req.params.id, {
                        $set: data
                    }).then((success) => {
                        res.json({
                            result: data,
                            status: true,
                            msg: "Banner updated successfully"
                        })
                    }).catch((err) => {
                        next({ status: 400, msg: "Banner could not be updated at this moment" })
                    })
                })
                .catch((error) => {
                    next({ status: 422, msg: JSON.stringify(error) })
                })
        } catch (error) {
            next({ status: 400, msg: JSON.stringify(error) })
        }
    }

    delete=(req,res,next)=>{
        BannerModel.findById(req.params.id)
        .then((banner)=>{
            if(banner){
                if(banner.image){
                    deleteImage(banner.image)
                }
                BannerModel.findByIdAndDelete(req.params.id)
                .then((success)=>{
                    res.json({
                        result: null,
                        status: true,
                        msg: "banner deleted successfully"
                    })
                })
                .catch((error)=>{
                    next({msg:JSON.stringify(error)})
                })
            }else{
                next({msg:"banner cound not be deleted", status:400})
            }
        })
        .catch((error)=>{
            next({msg: JSON.stringify(error)})
        })
    }
    getBanners = (req, res, next) => {
      BannerModel.find({
            status: req.params.status
        })
        .then((banners) => {
            if(banners) {
                res.json({
                    result: banners,
                    status: true,
                    msg: "All active Banners"
                })
            } else {
                res.json({
                    result: null,
                    status: false, 
                    msg: "No active banners"
                });
            }
        })
        .catch((errr) => {
            next({
                status: 500, msg: JSON.stringify(errr)
            })
        })
    }

}
module.exports = BannerController
