const express = require("express");
const app = express()
const Role = require("../model/role.model")
const authRoute = require("./auth.router")
const userRoute = require("./user.router")
const bannerRoute = require("./banner.router")
const brandRoute = require("./brand.router")
const categoryRoute = require('./category.router')
const productRoute = require('./product.router')
app.use('/auth',authRoute)
app.use('/user',userRoute)
app.use('/banner',bannerRoute)
app.use('/brand',brandRoute)
app.use('/category',categoryRoute)
app.use('/product',productRoute)
app.use('/roles',(req,res,next)=>{
    Role.find()
    .then((roles)=>{
        res.json({
            result: roles,
            status:200,
            msg:"roles found successfully"
        })
    })
    .catch((err)=>{
        next({msg:"roles not found"})
    })
})
module.exports = app