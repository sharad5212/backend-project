const Router = require("express").Router()
const ProductController = require('../controller/product.controller')
const productCtl = new ProductController()
const {loginCheck,adminAccess} = require('../middleware/auth.middleware')
const imageUploader = require('../middleware/imageUploader.middleware')
Router.route('/')
    .get(productCtl.getAllProduct)
    .post(loginCheck,adminAccess,imageUploader.single('image'),productCtl.addProduct)

Router.route('/:id')
    .get(productCtl.getProductById)
    .put(loginCheck,adminAccess,imageUploader.single('image'),productCtl.updateProduct)
    .delete(loginCheck,adminAccess,productCtl.deleteProduct)

module.exports = Router