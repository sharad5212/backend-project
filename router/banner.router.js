const Router = require("express").Router()
const BannerController = require("../controller/banner.controller")
const imageUploader = require("../middleware/imageUploader.middleware")
const bannerCtl = new BannerController()
const { loginCheck,adminAccess} = require("../middleware/auth.middleware")
Router.route('/')
    .get(loginCheck,adminAccess,bannerCtl.index)
    .post(loginCheck,adminAccess,imageUploader.single("image"),bannerCtl.store)

Router.route('/:id')
	.put(loginCheck,adminAccess, imageUploader.single('image'), bannerCtl.update)
    .get(bannerCtl.show)
  
    .delete(loginCheck,adminAccess,bannerCtl.delete)

Router.get('/status/:status',bannerCtl.getBanners);

module.exports = Router
