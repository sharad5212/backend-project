const Router = require('express').Router()
const CategoryController = require('../controller/category.controller')
const {loginCheck,adminAccess} = require('../middleware/auth.middleware')
const imageUploader = require("../middleware/imageUploader.middleware")
const categoryCtl = new CategoryController()
Router.route('/')
    .get(categoryCtl.index)
    .post(loginCheck,adminAccess,imageUploader.single('image'),categoryCtl.store)

Router.route('/:id')
    .get(loginCheck,adminAccess,categoryCtl.show)
    .put(loginCheck,adminAccess,imageUploader.single('image'),categoryCtl.update)
    .delete(loginCheck,adminAccess,categoryCtl.destroy)

module.exports = Router