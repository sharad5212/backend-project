const Router = require("express").Router();
const authController = require("../controller/auth.controller")
const imageUploader = require("../middleware/imageUploader.middleware")
const authCtl = new authController()

Router.post("/register",imageUploader.single("images"),authCtl.register)
Router.post("/login",authCtl.login)
Router.post("/forget",authCtl.forgetPassword)
Router.post("/reset",authCtl.resetPassword)
module.exports = Router
