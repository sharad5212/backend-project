const Router = require("express").Router()
const{loginCheck,adminAccess} = require('../middleware/auth.middleware')
const BrandController = require('../controller/brand.controller')
const imageUploader = require('../middleware/imageUploader.middleware')
const brandCtl = new BrandController()
Router.route('/')
    .get(loginCheck,adminAccess,brandCtl.index)
    .post(loginCheck,adminAccess,imageUploader.single('image'),brandCtl.store)

Router.route('/:id')
    .get(brandCtl.show)
    .put(loginCheck,adminAccess,imageUploader.single('image'),brandCtl.update)
    .delete(loginCheck,adminAccess,brandCtl.destroy)

Router.get('/status/:status',brandCtl.getBrand)
module.exports = Router