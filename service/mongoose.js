const mongoose = require("mongoose")
const config = require("../helper/config")

const url = config.DB_URL+"/"+config.DB_NAME

mongoose.connect(url)
.then((ack)=>{
    console.log("connect with db successfully")
})
.catch((err)=>{
    console.log("unable to connect with db")
})